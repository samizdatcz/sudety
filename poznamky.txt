1) Slovenská národnost, U111102302

2) Nezaměstnanost, U113100802

3) Parlamentní volby, účast

4) Hustota zalidnění

5) Potratovost (data v xls)

6) Zemědělská krajina (data v xls)



Německá národnost, U111102502

Nejvyšší ukončené vzdělání: základní, U112103602

Průměrné stáří obydlených domů celkem, U117103201

Zemědělská krajina (orp)

Lékaři na 1000 obyv. (kraje)

Kriminalita (okres)


1) parlamentní volby
	-
	- včetně účasti
	- zajímavý jsou jazykový ostrovy kolem Abertam, říkal Lindner

- etnická/národnostní struktura
	- http://helpdesk.migraceonline.cz/img/mapa-cizinci-pocet-zaku-big.png
	- severní Sudety jsou vidět
	- "Podle oficiálních československých dokumentů z roku 1950 přesídlilo, počítáme-li i optanty z někdejší Podkarpatské Rusi, do Československa 202 526 někdejších českých a slovenských emigrantů. Jejich naprostá většina se usídlila v někdejších Sudetech. Připočítáme-li k tomuto číslu kolem 200 000 zůstavších Němců, několik desítek tisíc Maďarů, zhruba 20 000 Řeků, 150 000 až 190 000 Slováků a desetitisíce východoslovenských Romů, kteří se v prvních letech po válce stěhovali za prací do českého pohraničí, zjistíme, že příslušníci těchto etnicky a kulturně odlišných skupin tvořili přibližně třetinu společnosti někdejších sudetských území, která koncem března 1947 čítala podle údajů ústředních osidlovacích orgánů 2 496 836 obyvatel." - http://www.antikomplex.cz/spolecnost-v-sudetech-po-roce-1945.html

- příjem
	- https://www.czso.cz/documents/11280/17990994/3.1E48_OpenElement&FieldElemFormat.gif/efff5aba-5e1c-4804-b915-beeb9dd94a0b
	- počet domácností pod hladinou chudoby je úplně jasnej

- kriminalita
	- http://www.mapakriminality.cz/
	- vyšší v severních Sudetech, ale ne v jižních

- nezaměstnanost
	- http://cdn.i0.cz/public-data/19/d0/5f1b68a0386ea40490935ed5de3a_w720_h458_g15da89c01ecb11e4bce90025900fea04.jpg?hash=a65ea4723efe94bb6a01b54389761b39
	- vyšší v severních Sudetech, ale ne v jižních

- zaniklé obce
	- https://www.google.com/maps/d/viewer?mid=zXKjYP6afxt0.k2sUmw2-2N4w&hl=en_US

- hustota zalidnění
	- https://www.czso.cz/documents/10180/20548145/4032120102.pdf/fc181875-7359-41cf-a232-405e9bb7add6?version=1.0
	- dosídleny byly severní (bohaté) Sudety, jižní Sudety ne; i na severu je hustota nerovnoměrná

- sňatečnost
	- "tam se to neřeší," říká masérka

- byty

- zalesnění a nezemědělská krajina
	- http://www.antikomplex.cz/zanik-zemedelske-krajiny-v-sudetech.html
	- "Nacházela se zde třetina orné půdy a více než tři čtvrtiny luk a pastvin, většina vinic, více než třetina lesů, velká část vodních ploch a rybníků. Až do vystěhování českých Němců ze země byla v Sudetech nejvyšší lidnatost v českých zemích." - http://magazinuni.cz/ruzne/antikomplex-aneb-jak-to-bylo-se-sudetami/

- populace před a po vyhnání (a nyní, jak se to dorovnávalo)
	- "Na konci roku 1952, který je označován za konec spontánního a hromadného osidlování, žily v pohraničí pouze dvě třetiny původního obyvatelstva z roku 1930. V roce 1959 vláda rozhodla všechny opuštěné domy a továrny zbourat, což se týkalo přes 34 tisíc objektů." - http://magazinuni.cz/ruzne/antikomplex-aneb-jak-to-bylo-se-sudetami/

- odsun v Polsku?

- koncentrace, národnost, odbornost lékařů?
	- "Katastrofální byl zejména nedostatek lékařů (například na Tachovsku byli v roce 1952 čtyři lékaři na 17 000 obyvatel) i porodních asistentek a s tím související úmrtnost novorozenců a kojenců (na zmíněném Tachovsku 124 dětí z 1000)" - http://www.antikomplex.cz/spolecnost-v-sudetech-po-roce-1945.html

- dobový tisk, články k Sudetům

- těžba uhlí

- německá  (a jiná) jména

- čeští a slovenští reemigranti (Volyně, Polsko, US, Belgie, Rumunsko...), Řekové a Makedonci

(věková struktura
prezidentská volba Zeman × Schwarzenberg - paradoxně zrovna v téhle volbě jsou proschwarzenbergovský http://nazory.ihned.cz/komentare/c1-61093790-ceska-historie-ve-volebnich-mistnostech)

---

"Navíc došlo k úplné ztrátě paměti, protože v letech 1952 až 1957 se změnila jména všech krajinných prvků v sudetských oblastech."

" zmizely kostely, hřbitovy, fary, kláštery, boží muka, sochy, cesty, restaurace, prameny."

"Bylo změněno 23 tisíc místních názvů"

"Je až s podivem, že o částečnou fyzickou záchranu Sudet se postaraly dva nesystémové jevy. Rozsáhlé vojenské újezdy zachránily na české straně Šumavy a Českého lesa alespoň krásné lesní ekosystémy, jejichž původní armádní využití ztratilo v nové Evropě smysl. A české chalupářství, národní masový jev, kupodivu stabilizovalo předindustriální sídelní strukturu venkova."

"http://magazinuni.cz/ruzne/antikomplex-aneb-jak-to-bylo-se-sudetami/"

---

"Dnes těžko říci, co nakonec více přispělo k opuštění rozsáhlých území, zda primárně násilné vylidnění, nebo likvidace pro venkov severních a západních Sudet charakteristických drobných průmyslových závodů. Jejich zařízení bylo ještě před rozhodnutím o odsunu už od prvních poválečných dnů rozebíráno a jako válečná kořist odváženo na východ. V každém případě na tom má podíl i únor 1948, který znamenal zánik soukromých zemědělců, malých podnikatelů a řemeslníků, jejichž způsob existence bezpečně vede k osvojení si místa do podoby domova."

"Pocit opuštěnosti se léty pod vlivem státního centralismu transformoval do pocitu cizí odpovědnosti za území a nároku na péči a požitky, které místní situace nenabízí. Zcela přesně to na počátku devadesátých let vyjádřil první polistopadový primátor Ústí nad Labem Lukáš Mašín, když prezidentu Václavu Havlovi řekl: „Všímejte si nás, nebo si budeme hledat vlastní identitu a spojence.“"

- http://www.antikomplex.cz/pribeh-krajiny-ktera-prisla-o-hospodare.html

---

"Pohraničí podle nich nemělo být nedokonalou nápodobou tradiční české společnosti, jak ji lidé znali z vnitrozemí. Mělo se naopak, jak vyplývá i z citovaných Kopeckého slov, stát předobrazem budoucnosti celého státu, tedy jakousi laboratoří společenské obrody. Pohraničí volalo nejlepší syny a dcery národa, bylo jedinečnou příležitostí vytvořit společnost oproštěnou od tradičních hierarchií a starých identit, které byly v nové době považovány za brzdu pokroku."

- http://www.antikomplex.cz/spolecnost-v-sudetech-po-roce-1945.html

---

"Tak jako tak se počítá s nižším osídlením, protože k asi 500 000 zůstavším Čechům má přibýt maximálně 1 200 000 Čechů z vnitrozemí. Plán rovněž rozděluje pohraničí na několik oblastí, pro něž určuje vždy trochu odlišnou podobu nové společnosti – a také různou hustotu osídlení. V severozápadních Čechách je třeba udržet průmysl, zemědělství ovšem jen v úrodných oblastech, oblast od Krkonoš po Jeseníky a jižní Morava mají být osídleny více méně v dosavadní hustotě, s důrazem na rekreační vyžití pro obyvatelstvo, Český les, Šumava a větší část jihočeského pohraničí mají být naopak osídlovány co nejméně, protože se jedná o „strategickou hranici prvního řádu“.5 Pozdější vývoj v pohraničí, který vedl mimo jiné k zániku či likvidaci stovek obcí a měst, tak evidentně nebyl pouze spontánní."

"Plány na kolonizaci pohraničí byly každopádně relativně racionální a směřovaly zejména k vyrovnání demografi ckého propadu a nahrazení stávajících pracovních sil. Nejprve bylo třeba obsadit veřejné úřady a dopravní podniky, pak průmyslové podniky a velké zemědělské závody (s přechodným ponecháním německého dělnictva), poté provést pozemkovou
reformu, aby se předešlo nedostatku potravin v zemi a zároveň byli do pohraničí nalákáni zemědělští osídlenci, nakonec plán počítal s osazením živností a svobodných povolání."

"Zatímco průmyslová města na severu řeší zejména kriminalitu a bytovou otázku, jsou základní demografi cké problémy, tedy naprostý nedostatek lidí, charakteristické pro horské oblasti, zejména na jihu a jihozápadě země. Již v roce 1947 zprávy z pohraničí upozorňují na to, že území politických okresů Tachov, Horšovský Týn, Domažlice, Klatovy, Sušice, Prachatice, Český Krumlov a Kaplice se nedaří doosídlit, že jsou tu celé liduprázdné osady a že v těchto oblastech je kolem 75 % obytných domů prázdných.8 Ačkoliv jedno z hesel tzv. odsunu německého obyvatelstva z území někdejších Sudet znělo „i kdyby v pohraničí měly růst kopřivy“,9 byly rozpadající se domy a chátrající kostely po Němcích již v roce 1947 častým předmětem kritiky."

"Jestliže nedostatek obyvatel byl charakteristický jen pro některé regiony někdejších Sudet, kvalifi kovaní lidé chyběli téměř všude. Týkalo se to zejména učitelů, lékařů, porodních asistentek, ale i dalších profesí."

"V letech 1945–1946 probíhala reemigrace samovolně a nebyla ještě státem organizována. V této fázi přijížděli do někdejších Sudet zejména potomci českých evangelíků z polského Zelova a vídeňští Češi. Krom toho byli demobilizováni vojáci z volyňské oblasti, jejichž rodiny se ovšem ještě zdržovaly na území Sovětského svazu. Zprvu přicházeli i emigranti z Francie, Belgie, či dokonce Spojených států, ale výměry přidělované půdy (zpravidla 13 ha) se zejména americkým krajanům zdály příliš malé, a tak jejich zájem brzy ustal. Celkem v této fázi přibylo do Československa, a to zejména do českého pohraničí, 64 000 novousedlíků z řad někdejších emigrantů a jejich potomků. 14. října 1946 byl zřízen reemigrační odbor ministerstva sociální péče a začala organizovaná etapa reemigrace. V letech 1947–1950 se do Československa stěhovali především volyňští Češi, kteří tvořili vyspělé zemědělské obyvatelstvo, a Češi a Slováci z Rumunska a Maďarska, většinou drobní zemědělci či nemajetní průmysloví dělníci, nepříliš vzdělaní a do značné míry akulturovaní v rumunském prostředí."

"Druhá významná skupina, Slováci, Češi a Rusíni z Rumunska, na rozdíl od volyňských Čechů nebyla v situaci, kdy by si mohla klást nějaké podmínky. Slovenské, případně české vesnice byly roztroušené po rumunských Karpatech, a tak této skupině chyběla jednotnější organizace. Čeští agitátoři z Československé přesídlovací komise Oradea-Mare jim naslibovali, že půjdou do lepšího, a tak prostě šli. Pochopitelně – to, že nejedou na Slovensko,  odkud jejich rody pocházely, se rumunští Slováci, kteří tvořili největší část této vlny „reemigrantů“, dozvěděli nejdříve ve vlaku. Desetitisíce těchto lidí (jen Slováků žilo v Rumunsku okolo 50 000, přičemž není jasné, kolik přesně jich přesídlilo do Sudet) byly rozvezeny do Jeseníků, do zapadlých koutů jižních Čech a jižní Moravy a do pustých oblastí západočeského pohraničí. Dostali většinou pouhé tři hektary půdy, přičemž se počítalo s tím, že se z nich stanou lesní dělníci. Někteří později odešli pracovat do severočeských továren."

"Jak známo, požadavek Československa na transfer slovenských Maďarů se nakonec do protokolu Postupimské konference nedostal a později jej spojenci – i vzhledem k často násilnému a nehumánnímu charakteru nuceného vysídlení Němců – již neakceptovali. Na základě dekretů prezidenta republiky však byli Maďaři zbaveni občanství, a tak i veškerých práv. Vedle výměny několika desítek tisíc z nich za Slováky žijící v Maďarsku a politiky reslovakizace se tak mohla začít praktikovat i politika tzv. vnitřní kolonizace. Tím se měly takříkajíc zabít dvě mouchy jednou ranou – jednak plán zapadal do záměru očistit jižní Slovensko od maďarského živlu a zároveň přispíval ke zvyšování počtu pracovních sil v někdejších Sudetech. Do pohraničí českých zemí tak bylo během tří měsíců (listopad 1946 – únor 1947) pod nátlakem a různými výhrůžkami přemístěno přibližně 45 000 Maďarů. Ti byli až do jara roku 1948 nuceni k setrvání na základě příslušného dekretu prezidenta republiky o pracovní povinnosti. Značná část z nich se po uplynutí této maximální lhůty, po kterou příslušný dekret umožňoval nutit lidi k práci v místě a za podmínek, jež si dotyční nevybrali a nepřáli, individuálně vrátila na jižní Slovensko.
Mezi lety 1948 a 1950 pak přicházeli do českého pohraničí Řekové a Makedonci, kteří utekli po prohrané občanské válce – zřejmě vůbec nejexotičtější doosídlenci. Jako první dorazily transporty dětí, které byly umístěny ve speciálně zřízených domovech, většinou v objektech po vyvlastněné a vyhnané německé šlechtě či buržoazii. K prvním štacím tak patřily zámky
v Lesné u Varnsdorfu, v Kyselce u Karlových Varů, v Loučné u Šumperka či v Chrastavě u Liberce. Rodiče, kteří dorazili o něco později, byli umístěni převážně v okresech Bruntál, Jeseník a Šumperk, částečně také v okrese Trutnov. Jen v dvacetitisícovém Krnově žilo v polovině padesátých let 2500 Řeků. Tito řečtí komunisté byli většinou zařazeni do práce jako lesní nebo průmysloví dělníci."

"Velká masa obyvatelstva bez tradic a ustálených  majetkových i jiných vztahů byla ideálním potenciálním voličstvem KSČ, která s těmito lidmi sdílela étos nového začátku a revolučních změn. To ostatně plně potvrdily volby na jaře roku 1946, kdy v pohraničí strana docílila volebního zisku 53 % (oproti průměrným 40 % v celých českých zemích)."

"Vysoké investice do industiálních center s sebou ovšem přinášely výrazné zanedbání jiných, zejména západních, jihozápadních a severovýchodních oblastí českého pohraničí. Venkovské kraje a venkovský život se krom toho nedaly konstruovat tak snadno jako moderní industriální společnost zaměřená na produkci. Zejména z odlehlejších oblastí začali zprvu z přídělů půdy nadšení a nyní čím dál tím méně spokojení zemědělci ve velkém odcházet již kolem roku 1948."

- http://www.antikomplex.cz/spolecnost-v-sudetech-po-roce-1945.html

---

http://www.washingtonpost.com/blogs/worldviews/wp/2014/10/31/the-berlin-wall-fell-25-years-ago-but-germany-is-still-divided/

---

http://www.usd.cas.cz/cs/spurny-matej
http://www.ustrcr.cz/cs/pavel-zeman

---

"Navíc nacisté, vedle obrovských ztrát, přinesli do země v rámci mašinerie vojenského průmyslu, jehož byl protektorát velice významnou součástí, ty absolutně nejvyspělejší technologie. Jen na okraj, právě v Česku se vyráběly už před koncem války první tryskové stíhačky světa, v té době tak zásadní výkřik technologie, jako jsou dnes například magnetické vlaky či před dvaceti lety raketoplány."

"Česká ekonomika se odsunem připravila o tři miliony většinou kvalifikovaných lidí, kteří navíc tvořili přirozený most k německé ekonomice, jež už mezi válkami byla pro Československo nejzásadnějším trhem. Tyto tři miliony vyhnaných a odsunutých „českých“ Němců se pak s dalšími miliony vyhnanců staly základem západoněmeckého hospodářského zázraku, protože ve zmenšeném Německu zcela pokryly ztráty pracovní síly, jež způsobila válka. Bavorsko dodnes oslavuje sudetské Němce jako hlavní tvůrce změny této ještě před válkou převážně zemědělské oblasti v jednu z nejbohatších a průmyslově nejvyspělejších částí nejen Německa, ale celé Evropy."

http://finmag.penize.cz/ekonomika/265719-na-odsun-a-vyhnani-tri-milionu-sudetskych-nemcu-doplaci-ceska-ekonomika-jeste-dnes

---

třebaže tu od 13. století žilo německé obyvatelstvo, které zde mělo většinu.

Masivní úbytek německého obyvatelstva Sudet byl alespoň z části nahrazen lidmi z vnitrozemí Českých zemí a Slovenska a také přistěhovalci ze zahraničí, například Řeky, Rumuny a Volyňskými Čechy.[12] Imigrantů z ciziny bylo úhrnem asi 200 000. Do Sudet později za podpory komunistické vlády přesídlilo i mnoho slovenských Cikánů a Maďarů, tehdejší tisk psal, že budou zdrojem pracovní síly v pohraničí.

https://cs.wikipedia.org/wiki/Sudety

---

Bohaté Sudety (brzy urbanizováno a rychle industrializováno
/textil, sklo/, do II. světové války zde dominovalo rozptýlené
venkovské osídlení, po válce odsun Němců s relativně
úspěšným dosídlením).

Chudé Sudety (nebylo nebo pozdě urbanizováno, do II.
světové války zde dominovalo rozptýlené venkovské osídlení,
po válce odsun Němců a spuštění „ţelezné opony“ - přerušení
přirozených přeshraničních kontaktů, nedosídleno, protoţe se
nejednalo o prioritní oblast zájmu: „první bojiště s NATO“ -
málo investic, ale cenné ţivotní prostředí; ztráta sociální
soudrţnosti a komunit)

https://is.muni.cz/auth/el/1441/podzim2014/Ze2RC_GOK3/um/SIDLA.pdf